import {useState, useEffect, useCallback} from 'react';

export const useGameStatus = rowsCleared => {
    const [score, setScore] = useState (0);
    const [rows, setrows] = useState (0);
    const [level, setLevel] = useState (0);

    const linePoints = [ 40, 100, 300, 1200];

    const calcScore = useCallback(()=> {
        // we have score
        if (rowsCleared > 0){
            //this is how original tettris score is calculated
            setScore(prev => prev +linePoints[rowsCleared - 1]* (level +1));
            setrows(prev => prev +rowsCleared);
        }
    }, [level, linePoints, rowsCleared])

    useEffect(()=> {
        calcScore();
    }, [calcScore, rowsCleared, score]);
    return[score, setrows, rows, setScore, level, setLevel];
}
