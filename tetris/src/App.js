import React from 'react';
import Tetris from './components/Tetris';

const App = (props) => (
<div className="App">
  <Tetris />
</div>

);

export default App;